'use strict';

const rpcCall = require('./rpc-call');
const Rational = require('tough-rational');

module.exports = (rpc, blockNumber) =>
  rpcCall(rpc, 'eth_getBlockByNumber', [
    !isNaN(blockNumber) ? Rational(blockNumber).toHexString() : blockNumber,
    false,
  ]);
