'use strict';

const {
  fromEthSale
} = require('ethereumjs-wallet');

module.exports = fromEthSale;
