'use strict';

const {
  fromV3
} = require('ethereumjs-wallet');

module.exports = fromV3;
