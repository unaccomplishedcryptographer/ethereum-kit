'use strict';

const soliditySha3 = require('./web3-utils/solidity-sha3');
const stripHexPrefix = require('./utils/strip-hex-prefix');
const getCode = require('./get-code');

const generateFunctionTest = (signature) => {
  const hashedSignature = stripHexPrefix(soliditySha3(signature)).substr(0, 8);
  return async (rpc, address) => Boolean((await getCode(rpc, address)).match(hashedSignature));
};

module.exports = generateFunctionTest;
