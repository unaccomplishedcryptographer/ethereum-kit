'use strict';

const flow = require('./internal/flow');
const ln = (v) => ((console.log(v)), v);
module.exports = flow(
  (rpc, provider) => rpc.setProvider(ln(provider))
)
