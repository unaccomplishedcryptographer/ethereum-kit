'use strict';

const {
  toAscii
} = require('web3-utils');

module.exports = toAscii;
