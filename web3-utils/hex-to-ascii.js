'use strict';

const {
  hexToAscii
} = require('web3-utils');

module.exports = hexToAscii;
