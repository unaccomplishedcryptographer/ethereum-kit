'use strict';

const {
  fromAscii
} = require('web3-utils');

module.exports = fromAscii;
