'use strict';

const {
  leftPad
} = require('web3-utils');

module.exports = leftPad;
