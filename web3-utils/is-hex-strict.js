'use strict';

const {
  isHexStrict
} = require('web3-utils');

module.exports = isHexString;
