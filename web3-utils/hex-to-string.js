'use strict';

const {
  hexToString
} = require('ethereumjs-util');

module.exports = hexToString;
