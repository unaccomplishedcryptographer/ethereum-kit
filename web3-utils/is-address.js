'use strict';

const {
  isAddress
} = require('web3-utils');

module.exports = isAddress;
