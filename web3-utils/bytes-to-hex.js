'use strict';

const {
  bytesToHex
} = require('web3-utils');

module.exports = bytesToHex;
