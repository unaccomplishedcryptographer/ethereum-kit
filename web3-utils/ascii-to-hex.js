'use strict';

const {
  asciiToHex
} = require('web3-utils');

module.exports = asciiToHex;
