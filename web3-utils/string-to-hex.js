'use strict';

const {
  stringToHex
} = require('web3-utils');

module.exports = stringToHex;
