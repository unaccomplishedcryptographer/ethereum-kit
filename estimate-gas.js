'use strict';

const wrap = require('./wrap');

module.exports = wrap.wrapNumber(require('./rpc-method')('eth_estimateGas'));
