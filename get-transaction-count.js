'use strict';

const getTransactionCountHex = require('./get-transaction-count-hex');
const wrap = require('./wrap');

module.exports = wrap.wrapNumber(getTransactionCountHex);
