'use strict';

const makeTransaction = require('./make-transaction');
const getTransactionCount = require('./get-transaction-count');

const fetchNonceMakeTransaction = async (rpc, tx) => {
  const nonce = await getTransactionCount(rpc, tx.from);
  const retval = makeTransaction(rpc, tx);
  retval.nonce = nonce;
  return retval;
};

module.exports = fetchNonceMakeTransaction;
  
