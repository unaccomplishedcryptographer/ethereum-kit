'use strict';

const uncurryThis = require('./uncurry-this');

const applyFunction = uncurryThis(Function.prototype.apply);

module.exports = applyFunction;
