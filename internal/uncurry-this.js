'use strict';
const head = require('tough-rational/lib/util/first');
const tail = require('tough-rational/lib/util/tail');

const uncurryThis = (fn) => (...args) => fn.apply(head(args), tail(args));

module.exports = uncurryThis;
