'use strict';

const propSetter = (prop) => (o, v) => {
  o[prop] = v;
  return o;
};

module.exports = propSetter;
