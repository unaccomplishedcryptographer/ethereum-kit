'use strict';

const {
  substr,
  split,
  toUpperCase
} = String.prototype;
const {
  join,
  map
} = Array.prototype;

const capitalizeFirst = (s) => toUpperCase.call(substr.call(s, 0, 1)) + substr.call(s, 1);

const hyphenToCamelCase = (s) => join.call(map.call(split.call(String(s), '-'), (segment, i) => i === 0 ? segment : capitalizeFirst(segment)), '');

module.exports = hyphenToCamelCase;
