'use strict';

const fs = require('fs');
const path = require('path');
const hyphenToCamelCase = require('./hyphen-to-camelcase');
const castArray = require('./cast-array');

const { reduce } = Array.prototype;
const { match } = String.prototype;

const requireAll = (pathname, exclude) => {
  const excludeCasted = castArray(exclude);
  return reduce.call(fs.readdirSync(pathname), (r, v) => {
    if (reduce.call(excludeCasted, (r, filename) => r || Boolean(match.call(v, filename)), false)) return r;
    r[hyphenToCamelCase(path.parse(v).name)] = require(path.join(pathname, v));
    return r;
  }, {});
};

module.exports = requireAll;
  
