'use strict';

const mapOwnNonEnumerable = (fn) => (o) => Object.getOwnPropertyNames(o).reduce((r, v) => {
  r[v] = fn(o[v], v, o);
  return r;
}, {});

module.exports = mapOwnNonEnumerable;
