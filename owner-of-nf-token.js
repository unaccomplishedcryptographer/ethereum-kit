'use strict';

const callToAddress = require('./call-to-address');
const encodeFunctionCall = require('./abi/encode-function-call');

const ownerOfNFT = async (rpc, to, nft) => callToAddress(rpc, {
  to,
  data: encodeFunctionCall({
    name: 'ownerOf',
    inputs: [{
      type: 'uint256',
      name: 'token'
    }]
  }, [ nft ])
});

module.exports = ownerOfNFT;
