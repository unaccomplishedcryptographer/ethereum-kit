'use strict';

const toBuffer = require('./to-buffer');
const {
  Buffer: {
    from: bufferFrom,
    concat: bufferConcat
  }
} = require('safe-buffer');

const isValidPublic = (publicKey, sanitize = false) => {
  publicKey = toBuffer(publicKey);
  if (publicKey.length === 64) return secp256k1.publicKeyVerify(bufferConcat([bufferFrom([4]), publicKey]));
  if (!sanitize) return false;
  return secp256k1.publicKeyVerify(publicKey);
};

module.exports = isValidPublic;
