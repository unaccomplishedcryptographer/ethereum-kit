'use strict';

const getUnit = require('./get-unit');
const Rational = require('tough-rational');
const flow = require('../internal/flow');

module.exports = flow(getUnit, Rational);
