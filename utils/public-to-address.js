'use strict';

const secp256k1 = require('secp256k1');
const toBuffer = require('./to-buffer');
const assert = require('../internal/assert');
const keccak = require('./keccak');

const publicToAddress = (pubKey, sanitize) => {
  pubKey = toBuffer(pubKey);
  if (sanitize && pubKey.length !== 64) {
    pubKey = secp256k1.publicKeyConvert(pubKey, false).slice(1);
  }
  assert(pubKey.length === 64);
  return keccak(pubKey).slice(-20);
};

module.exports = publicToAddress;
