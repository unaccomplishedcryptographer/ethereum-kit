'use strict';

const bindKey = require('../internal/bind-key');
module.exports = bindKey(/^0x[0-9a-fA-F]*$/, 'test');
