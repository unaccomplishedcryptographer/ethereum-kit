'use strict';

const toBuffer = require('./to-buffer');

const privateToPublic = (privateKey) => secp256k1.publicKeyCreate(toBuffer(privateKey), false).slice(1);

module.exports = privateToPublic;
