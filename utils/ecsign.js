'use strict';

const secp256k1 = require('secp256k1');
const mapToHexMaybe = require('../internal/map-to-hex-maybe');
const toBuffer = require('./to-buffer');

const ecsign = (msgHash, privateKey) => {
  const {
    signature,
    recovery
  } = secp256k1.sign(toBuffer(msgHash), toBuffer(privateKey));
  return mapToHexMaybe({
    v: recovery + 27,
    r: signature.slice(0, 32),
    s: signature.slice(32, 64)
  });
};

module.exports = ecsign;
