'use strict';

const isValidAddress = require('./is-valid-address');
const toChecksumAddress = require('./to-checksum-address');
const isValidChecksumAddress = (address) => isValidAddress(address) && toChecksumAddress(address) === address;

module.exports = isValidChecksumAddress;
