'use strict';

const Rational = require('tough-rational');
const { isRational } = require('tough-rational/lib/fixtures');
const isHexString = require('./is-hex-string');
const stripHexPrefix = require('./strip-hex-prefix');
const intToBuffer = require('./int-to-buffer');
const padToEven = require('./pad-to-even');
const {
  Buffer: {
    from: bufferFrom,
    isBuffer,
    allocUnsafe
  }
} = require('safe-buffer');
const { isArray } = Array;

const toBuffer = (v) => {
  switch (true) {
    case isRational(v):
      return bufferFrom(stripHexPrefix(v.toHexString()), 'hex');
    case isBuffer(v):
      return v;
    case isArray(v):
      return bufferFrom(v);
    case typeof v === 'string':
      return isHexString(v) ? bufferFrom(padToEven(stripHexPrefix(v)), 'hex') : bufferFrom(v);
    case typeof v === 'number':
      return intToBuffer(v);
    case v == null:
      return allocUnsafe(0);
    case Boolean(v.toArrayLike):
      return v.toArrayLike(Buffer);
    case Boolean(v.toArray):
      return bufferFrom(v.toArray());
    default:
      throw new Error('invalid type');
  }
};

module.exports = toBuffer;
