'use strict';

const zeroAddress = require('./zero-address');
const equals = require('../internal/equals');

module.exports = equals(zeroAddress());
