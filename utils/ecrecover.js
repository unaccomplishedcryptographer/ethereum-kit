'use strict';
const secp256k1 = require('secp256k1');
const setLength = require('./set-length');
const toBuffer = require('./to-buffer');
const bufferToHex = require('./buffer-to-hex');
const publicToAddress = require('./public-to-address');
const ecrecover = require('./ecrecover-raw');
const {
  Buffer: {
    concat
  }
} = require('safe-buffer');

module.exports = (hash, v, r, s) => bufferToHex(publicToAddress(ecrecover(toBuffer(hash), v, toBuffer(r), toBuffer(s))));
