'use strict';

const keccak = require('./keccak');
const rlp = require('rlp');
const bindKey = require('../internal/bind-key');
const flow = require('../internal/flow');

module.exports = flow(bindKey(rlp, 'encode'), keccak);
