'use strict';

const toBuffer = require('./to-buffer');
const secp256k1 = require('secp256k1');

const importPublic = (publicKey) => {
  publicKey = toBuffer(publicKey);
  return publicKey.length !== 64 ? secp256k1.publicKeyConvert(publicKey, false).slice(1) : publicKey;
};

module.exports = importPublic;
