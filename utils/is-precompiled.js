'use strict';

const unpad = require('./unpad');

const isPrecompiled = (address) => {
  const a = unpad(address);
  return a.length === 1 && a[0] >= 1 && a[0] <= 8;
};

module.exports = isPrecompiled;
