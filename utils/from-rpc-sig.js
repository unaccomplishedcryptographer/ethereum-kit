'use strict';

const toBuffer = require('./to-buffer');
const mapToHexMaybe = require('../internal/map-to-hex-maybe');

const fromRpcSig = (sig) => {
  sig = toBuffer(sig);
  if (sig.length !== 65) throw new Error('Invalid signature length');
  let v = sig[64];
  if (v < 27) v += 27;
  return mapToHexMaybe({
    v,
    r: sig.slice(0, 32),
    s: sig.slice(32, 64)
  });
};

module.exports = fromRpcSig;
