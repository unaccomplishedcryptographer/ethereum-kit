'use strict';

const bindKey = require('../internal/bind-key');

const isValidAddress = bindKey(/^0x[0-9a-fA-F]{40}$/, 'test');

module.exports = isValidAddress;
