'use strict';

const stripHexPrefix = require('./strip-hex-prefix');
const keccak = require('./keccak');
const { reduce } = [];
const { toUpperCase } = String.prototype;

const toChecksumAddress = (address) => {
  address = stripHexPrefix(address).toLowerCase();
  const hash = keccak(address).toString('hex');
  return reduce.call(address, (r, v, i) => parseInt(hash[i], 16) >= 8 ? r + toUpperCase.call(v) : r + v, '0x');
};

module.exports = toChecksumAddress;
