'use strict';

const addHexPrefix = (s) => String(s).substr(0, 2) === '0x' ? String(s) : '0x' + String(s);

module.exports = addHexPrefix;
