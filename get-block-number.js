'use strict';

const wrap = require('./wrap');
const getBlockNumberHex = require('./get-block-number-hex');

module.exports = wrap.wrapNumber(getBlockNumberHex);
