'use strict';

const requireAll = require('../../internal/require-all');
const forOwn = require('../../internal/for-own');

module.exports = [
  'ethereum-sdk internal modules',
  it => () =>
    forOwn(requireAll(__dirname, 'index.js'), ([description, suite]) =>
      it(description, suite),
    ),
];
