'use strict';

const fs = require('fs');
const path = require('path');
require('chai').use(require('chai-spies'));

fs.readdirSync(__dirname).forEach((v) => {
  if (fs.lstatSync(path.join(__dirname, v)).isDirectory()) {
    const [ description, suite ] = require(path.join(__dirname, v));
    describe(description, suite(it));
  }
});
