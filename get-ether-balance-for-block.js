'use strict';

const getEtherBalanceForBlockHex = require('./get-ether-balance-for-block-hex');
const wrap = require('./wrap');

module.exports = wrap.wrapRational(getEtherBalanceForBlockHex);
