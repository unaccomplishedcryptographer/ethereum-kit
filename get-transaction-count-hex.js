'use strict';

const partialRight = require('./internal/partial-right');
const getTransactionCountForBlockHex = require('./get-transaction-count-for-block-hex');

module.exports = partialRight(getTransactionCountForBlockHex, 'pending');
